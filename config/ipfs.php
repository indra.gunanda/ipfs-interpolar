<?php

return [

    'ip'      => env('IPFS_IP', 'localhost'),
    'port'    => env('IPFS_PORT', '8080'),
    'apiport' => env('IPFS_API_PORT', '5001'),

];
